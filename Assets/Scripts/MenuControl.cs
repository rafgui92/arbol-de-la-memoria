﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuControl : MonoBehaviour
{
     float timeLeft = 30.0f;
     
    // Start is called before the first frame update
    public void Empezar(string AMAR)
    {
        print ("AM_AR"+ AMAR);
        SceneManager.LoadScene(AMAR);
    }
    public void Principal(string Principal)
    {
        print ("Principal Menu");
        SceneManager.LoadScene(Principal);
    }

    public void Rec(string Recording)
    {
        print ("Graba tu memoria "+ Recording);
        SceneManager.LoadScene(Recording);
    }

    public void statistics ()
    {
         Application.OpenURL("https://drive.google.com/file/d/1BMP7mKPMEf2FUYLopevRf8VOQA5v3ZIm/view");
    }
     public void D1_2 ()
    {
         Application.OpenURL("http://www.idep.edu.co/wp_centrovirtual/wp-content/uploads/2015/12/1797%20-%20Cartilla%20Laconica%20de%20las%20quatro%20reglas%20de%20aritmetica%20practica.pdf");
    }
    public void D2_2_0 ()
    {
         Application.OpenURL("http://www.idep.edu.co/wp_centrovirtual/wp-content/uploads/2015/12/1845%20-%20Manual%20de%20Ensenanza%20Mutua%20para%20las%20Escuelas%20de%20primeras%20letras.pdf");
    }
    public void D2_2_1 ()
    {
         Application.OpenURL("http://bit.ly/38uzzdq");
    }
    public void D2_2_2 ()
    {
         Application.OpenURL("http://www.idep.edu.co/wp_centrovirtual/wp-content/uploads/2015/12/1846%20-%20Manual%20Gramatica%20Metodo%20Pestalozzi.pdf");
    }
    public void D4_2_0 ()
    {
         Application.OpenURL("https://www.mineducacion.gov.co/1621/articles-102524_archivo_pdf.pdf");
    }
    public void D4_2_1 ()
    {
         Application.OpenURL("https://www.mineducacion.gov.co/1759/articles-102506_archivo_pdf.pdf");
    }
    public void D3_2 ()
    {
         Application.OpenURL("https://revistas.pedagogica.edu.co/index.php/RCE/article/view/5024/4110");
    }
    public void D7_2 ()
    {
         Application.OpenURL("https://www.mineducacion.gov.co/1621/articles-103714_archivo_pdf.pdf");
    }

    public void Exit ()
    {
        Application.Quit();
    }

    ///

    public void Timmer(string H1_1)
     {
         timeLeft -= Time.deltaTime;
         if(timeLeft < 0)
         {
            print ("Escenas RA "+ H1_1);
            SceneManager.LoadScene(H1_1);
         }
     }

}
