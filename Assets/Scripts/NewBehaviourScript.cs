﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewBehaviourScript : MonoBehaviour
{

    public float tiempo;
    public string nivel;

    void Awake()
    {
        //Debug.Log("Awake");
    }

    void Start()
    {
        //Debug.Log("Example1");
    }

    void Update()
    {
        while(tiempo > 0)
        {
            tiempo--;
        }
        if(tiempo == 0){
            SceneManager.LoadScene(nivel);
        }
    }
}
