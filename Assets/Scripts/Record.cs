﻿using UnityEngine;
using AudioRecorder;

public class Record : MonoBehaviour {
	 
	Recorder recorder;
	AudioSource audioSource;

	public bool autoPlay;

	string log = "";

	void OnEnable()
	{
		recorder = new Recorder();
		Recorder.onInit += OnInit;
		Recorder.onFinish += OnRecordingFinish;
		Recorder.onError += OnError;
		Recorder.onSaved += OnRecordingSaved;
	}
	void OnDisable()
	{
		Recorder.onInit -= OnInit;
		Recorder.onFinish -= OnRecordingFinish;
		Recorder.onError -= OnError;
		Recorder.onSaved -= OnRecordingSaved;
	}

	//Use this for initialization  
	void Start()   
	{  

		audioSource = GameObject.FindObjectOfType<AudioSource>();
		recorder.Init();
	}  
	
	void OnGUI()   
	{  
		GUILayout.Label (log);

		if(recorder.IsReady)  
		{  
			if(!recorder.IsRecording)  
			{  
				GUIStyle buttonStyle = new GUIStyle(GUI.skin.button);
				buttonStyle.fontSize = 18;
				buttonStyle.fontStyle = FontStyle.Bold;

				if(GUI.Button(new Rect(Screen.width/2-150, Screen.height/2-100, 300, 60), "Graba tu memoria pedagogica", buttonStyle))  
				{  
					recorder.StartRecording(false,60);
				}  
			}  
			else
			{  
				GUIStyle buttonStyle = new GUIStyle(GUI.skin.button);
				buttonStyle.fontSize = 18;
				buttonStyle.fontStyle = FontStyle.Bold;
				if(GUI.Button(new Rect(Screen.width/2-150, Screen.height/2-100, 300, 60), "Parar",buttonStyle))  
				{  
					recorder.StopRecording();
				}   
				
				//GUI.Label(new Rect(Screen.width/2-150, 50, 300, 60), "Recording...");  
			} 

			if(recorder.hasRecorded)
			{
				GUIStyle buttonStyle = new GUIStyle(GUI.skin.button);
				buttonStyle.fontSize = 16;
				buttonStyle.fontStyle = FontStyle.Bold;
				if(GUI.Button(new Rect(Screen.width/2-150, Screen.height/2-30, 300, 60), "Reproducir tu memoria",buttonStyle))  
				{  
					recorder.PlayRecorded(audioSource);
				} 
				if(GUI.Button(new Rect(Screen.width/2-150, Screen.height/2+40, 300, 60), "Guarda tu memoria para los demas", buttonStyle))  
				{  
					recorder.Save(System.IO.Path.Combine(Application.persistentDataPath,"Audio"+Random.Range(0,10000)+".wav"),recorder.Clip);
				} 
			}
		}  
	}  

	void OnInit(bool success)
	{
		Debug.Log("Success : "+success);
		log += "\nSuccess";
	}

	void OnError(string errMsg)
	{
		Debug.Log("Error : "+errMsg);
		log += "\nError " + errMsg;
	}

	void OnRecordingFinish(AudioClip clip)
	{
		if(autoPlay)
		{
			recorder.PlayAudio (clip, audioSource);

			// or you can use
			//recorder.PlayRecorded(audioSource);
		}
	}

	void OnRecordingSaved(string path)
	{
		Debug.Log("File Saved at : "+path);
		log += "\nFile save at : "+path;
		recorder.PlayAudio (path, audioSource);
	}
}
