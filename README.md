# Arbol de la memoria
======================

Este archivo contiene la informacion de contenido link y uso del repositorio dispuesto a entrega para SED con el fin de que el usuario final pueda modificar, cambiar o respaldar este codigo con el permiso de la SED o el usuario final y el dueño del repositorio.

- [Contenido](#Contenido)
- [Versiones de compilacion](#Versiones-de-compilacion)
- [Requerimiento de equipos](#Requerimiento-de-equipos)
- [EasyAR](#EasyAR)
- [Politicas de repositorio](#Politicas-de-repositorio)

## Versiones de compilacion ##
En el repositorio encontramos una carpeta de versiones de compilación donde encontramos el código compilado, de los trabajos realizados ademas de la version para ejecución de forma portable o sin permisos de instalación, ademas encontraremos un archivo de ejecución de instalación para instalación en equipos windows.

**La carpeta contiene:**

    -   AMx86 (EXE)
    -   AMx86vPortable (EXE, DLL, Liberias, ETC)

## Requerimiento de equipos ##
En el software corre en una version de 32bit con un drive de windows de x86 ademas los requerimiento del equipo en el cual se instalara la aplicacion debe ser:

-   ***Sistema Operativo:*** Windows 7 o superior (64/32 Bits)
-   ***Ram:*** 500 MB.
- ***Espacio en disco:*** 400MB.
- ***Espacio de Setup (Instalador):*** 130MB.
- ***Idioma:*** Español




## Contenido ##

**El repositorio a continuon contiene los siguientes archivos:**

    -   Codigo de UNITY version 2019.2.17f1
    -   Archivos EXE, DLL de publicacion de la version 1.0
    
## EasyAR ##

La motor Realidad Aumentada (AR) por sus siglas en ingles, API desarrollado por la compañia VisionStar Information Technology en shangai, es un motor de AR que a tomado fuerza en los ultimos años desde empresas chinas como empresas como PEPSI, KFC, VOGUE entre otras aplicacion y juegos desarrolladas en este motor para mas informacion sobre [EasyAR](https://www.easyar.com).

La informacion brindada para el uso del API es la siguiente:

### SDK License Key
    AxK/yQcBp9UfYJ03chGTQQUeUtAeKcBmKauXsjMgieIHMI//Mz2YsnxxnvEgNJn5MTaO4y8nidAhPo35Kn2P/ytxwLIrMp/kIyGn9T8aiLJ8YsCyKjqP9SggieNkabfrZDGZ/iI/idkiIM6qHXGP/yt9nvEgNJn5aDKe8ik/iPUqMoH1Kzye+SdxwLIlPIG+NDKK9zM6wvE0MYP8IjaA8Ss2gf80Oo2yG3/O5ichhfEoJ5+yfAjO8icghfNkDsCyNj+N5CA8nv01cdbLZCSF/iI8m+Nkf879JzDOzWpxieg2Op71EjqB9RUnjf02cdb+Mz+AvGQ6n9wpMI38ZGmK8Sogie1qKM7yMz2I/CMaiONkabeyJTyBvjQyivczOsLxNDGD/CI2gPErNoH/NDqNsht/zuYnIYXxKCefsnwIzvInIIXzZA7AsjY/jeQgPJ79NXHWy2QygvQ0PIX0ZA7AsiMrnPk0Nrj5Kza/5Cc+nLJ8PZn8Kn/O+TUfg/MnP86qIDKA4yMuwOtkMZn+Ij+J2SIgzqodcY//K32e8SA0mfloMp7yKT+I9SoygfUrPJ75J3GxvGQljeIvMoLkNXHWy2QxjeMvMM7NanGc/Ccniv80Pp+yfAjO+Skgzs1qcYnoNjqe9RI6gfUVJ439NnHW/jM/gLxkOp/cKTCN/GRpivEqIIntGy6BOAUX090BsxLREveifDrzSfPqmLxjsgfcaEr4TArgargYdeOtPjGbaQdo/d3Otqjc4TQMXPqJv1rpxQsz69ev++8rp0Q7nFL6MNatYGbIhUAkzC0sJgpJtiIzFZaepaK80H0ouaHuXFHTU0lC9SP45YbNafOIHGTiz4lf0LNM8VJE3nEkRKZbGsymz3VW8E+3fvxiKhxc7quM1K23BeC/mVrghA2Q7iK67aRiSxHWfUYtTrcku8G9lQFUoUAE0sGwWWB9OlTFI4Akj+kKllFP4yScNy0gCAW+aj/8hIYTfrO4xKHZ+RskddB+/56dRQpEU+edWi3F2hnxgR5GU+yQ

### Bundle ID

| **IOS** : com.rafgui.arboldelamemoria |
| **Android** : com.rafgui.arboldelamemoria |

## Politicas de repositorio

Esta REPOSITORIO se entrega en mutuo acuerdo de satisfacción, si por algún motivo no se llega a este acuerdo de satisfaccion este código le pertenece, para uso y comercializacion del desarrollador de la app o dueño del repositorio.